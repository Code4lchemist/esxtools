#!/usr/bin/perl -w

use utf8;
use Text::Unidecode;
use strict;
use warnings;

use VMware::VIRuntime;
use VMware::VILib;

my %opts = (
        'vmname' => {
        		type => "=s",
			help => "The name of the virtual machine to apply operation",
        		required => 1,
        },
        'force' => {
	        	type => "=s",
        		help => "Force: [yes, no]. \n" .
			"\tThe application will poweroff the machine if operation = shutdown\n" .
			"\tand vmware-tools are not available.\n" .
			"\tExample: power_vm.pl --vmname ldvunxbuild01 --operation h --force yes",
	        	required => 0,
			default => "no",
        },
        'operation' => {
	        	type => "=s",
        		help => "The power opwerations to be perfomed on the VMs: \n" .
			"\tshutdown  #Shutdown the OS\n" .
			"\treboot    #Reboot the OS\n" .
			"\treset     #Hard reboot the VM\n" .
			"\tpoweron   #Power on the VM\n" .
		       	"\tpoweroff  #Power off the VM\n" .
			"\tstatus    #Display power status\n" .
			"\tThe default operation = status.\n" .
			"\tExample:  power_vm.pl --vmname ldvunxbuild01 --operation h",
	        	required => 0,
			default => "status",
        },
);

my %operations = (
	'status' => "",
	'shutdown' => "",
	'reboot' => "",
	'reset' => "",
	'poweron' => "",
	'poweroff' => ""
);

Opts::add_options(%opts);

# validate options, and connect to the server
Opts::parse();
Opts::validate(\&validate);
Util::connect();

my $vmname  = Opts::get_option('vmname');
my $force  = Opts::get_option('force');
my $op = Opts::get_option('operation');

my $vm_views = Vim::find_entity_views(view_type => 'VirtualMachine',
				    properties => ['name', 'guest', 'config', 'runtime'], 
				    filter =>{ 'name' => qr/$vmname/});

if ($vm_views) {
	foreach my $vm_view (@$vm_views) {
		my $vm_name = $vm_view->name;
		print "VM [ " . lc($vm_name) . " ]\n";

		if( $op eq "status" ) {
			power_status($vm_view);
		}
		elsif( $op eq "shutdown" ) {
			if ( ($vm_view->guest->toolsStatus->val =~ "Not") && (lc($force) eq "yes" ) ){
				&poweroff_vm($vm_view);
			} else {
				&shutdown_guest($vm_view);
			}
		}
		elsif( $op eq "poweron" ) {
			poweron_vm($vm_view);
		}
		elsif( $op eq "poweroff") {
			poweroff_vm($vm_view);
		}
		elsif( $op eq "reboot") {
			reboot_guest($vm_view);
		}
		elsif( $op eq "reset") {
			reset_vm($vm_view);
		}

		print "\n";
	}
}

Util::disconnect();

sub power_status {
	my ($vm_view) = @_;

	my $pwrStatus = $vm_view->runtime->powerState->val;
	my $nCPUs = $vm_view->config->hardware->numCPU;
	my $nCoresPerSocket = $vm_view->config->hardware->numCoresPerSocket;
	my $memory  = scaleIt($vm_view->config->hardware->memoryMB * 1048576); 

	print "  +HARDWARE\n";
	print "     --> No CPUs      = " . $nCPUs  . "\n";
	print "     --> Cores/Socket = " . $nCoresPerSocket  . "\n";
	print "     --> Memory Size  = " . $memory . "\n";
	print "     --> Power Status = " . $pwrStatus  . "\n";
}

sub shutdown_guest {
	my ($vm_view) = @_;
	eval {
		$vm_view->ShutdownGuest();
		Util::trace (0, "\n--> Start shutdown process on '" .  $vm_view->name . "'\n");
	};
	if ($@) {
		if (ref($@) eq 'SoapFault') {
			Util::trace (0, "Error in '" .  $vm_view->name . "':\n");
			unless (print_error(ref($@->detail)) == 1) {
				Util::trace(0, "\tVM '" . $vm_view->name . "' can't be shutdown \n"
					       . $@ . "\n");
			}
		}
		else {
			Util::trace(0, "\tVM '" . $vm_view->name . "' can't be shutdown \n"
				       . $@ . "\n");
		}
	}
	else {
		Util::trace (0, "\n----> Shutdown process on '" .  $vm_view->name . "' completed\n");
	}
}

sub poweron_vm {
	my ($vm_view) = @_;
	eval {
		$vm_view->PowerOnVM();
		Util::trace (0, "\n--> Start poweron process on '" .  $vm_view->name . "'\n");
	};
	if ($@) {
		if (ref($@) eq 'SoapFault') {
			Util::trace (0, "Error in '" .  $vm_view->name . "':\n");
			unless (print_error(ref($@->detail)) == 1) {
				Util::trace(0, "\tVM '" . $vm_view->name . "' can't be powered on \n"
					       . $@ . "\n");
			}
		}
		else {
			Util::trace(0, "\tVM '" . $vm_view->name . "' can't be powered on \n"
				       . $@ . "\n");
		}
	}
	else {
		Util::trace (0, "\n----> Poweron process on '" .  $vm_view->name . "' completed\n");
	}
}

sub poweroff_vm {
	my ($vm_view) = @_;
	eval {
		$vm_view->PowerOffVM();
		Util::trace (0, "\n--> Start poweroff process on '" .  $vm_view->name . "'\n");
	};
	if ($@) {
		if (ref($@) eq 'SoapFault') {
			Util::trace (0, "Error in '" .  $vm_view->name . "':\n");
			unless (print_error(ref($@->detail)) == 1) {
				Util::trace(0, "\tVM '" . $vm_view->name . "' can't be powered off \n"
					       . $@ . "\n");
			}
		}
		else {
			Util::trace(0, "\tVM '" . $vm_view->name . "' can't be powered off \n"
				       . $@ . "\n");
		}
	} 
	else {
		Util::trace (0, "\n----> Poweroff process on '" .  $vm_view->name . "' completed\n");
	}
}

sub reboot_guest {
	my ($vm_view) = @_;
	eval {
		$vm_view->RebootGuest();
		Util::trace (0, "\n--> Start reboot guest process on '" .  $vm_view->name . "'\n");
	};
	if ($@) {
		if (ref($@) eq 'SoapFault') {
			Util::trace (0, "Error in '" .  $vm_view->name . "':\n");
			unless (print_error(ref($@->detail)) == 1) {
				Util::trace(0, "\tVM '" . $vm_view->name . "' can't be rebooted \n"
					       . $@ . "\n");
			}
		}
		else {
			Util::trace(0, "\tVM '" . $vm_view->name . "' can't be rebooted \n"
				       . $@ . "\n");
		}
	} 
	else {
		Util::trace (0, "\n----> Reboot guest process on '" .  $vm_view->name . "' completed\n");
	}
}

sub reset_vm {
	my ($vm_view) = @_;
	eval {
		$vm_view->ResetVM();
		Util::trace (0, "\n--> Start reset vm process on '" .  $vm_view->name . "'\n");
	};
	if ($@) {
		if (ref($@) eq 'SoapFault') {
			Util::trace (0, "Error in '" .  $vm_view->name . "':\n");
			unless (print_error(ref($@->detail)) == 1) {
				Util::trace(0, "\tVM '" . $vm_view->name . "' can't be reset \n"
					       . $@ . "\n");
			}
		}
		else {
			Util::trace(0, "\tVM '" . $vm_view->name . "' can't be reset \n"
				       . $@ . "\n");
		}
	} 
	else {
		Util::trace (0, "\n----> Reset vm process on '" .  $vm_view->name . "' completed\n");
	}
}

sub print_error {

	my ($error) = @_;

	if ($error eq 'InvalidState') {
		Util::trace(0, "\tCurrent State of the VM is not supported for this operation.\n");
		return 1;
	}
	elsif ($error eq 'InvalidPowerState') {
		Util::trace(0, "\tThe attempted operation cannot be performed in the current state.\n");
		return 1;
	}
	elsif ($error eq 'NotSupported') {
		Util::trace(0, "\tThe operation is not supported on the object.\n");
		return 1;
	}
	elsif ($error eq 'ToolsUnavailable') {
		Util::trace(0, "\tVMTools are not running in this VM.\n");
		return 1;
	}

	return 0;
}

sub scaleIt {
	my ($size, $n) = (shift, 0);
	++$n and $size /= 1024 until $size < 1024;
	return sprintf "%.2f %s", $size, (qw[ bytes KB MB GB TB ] )[ $n ];
}

sub validate {
	my $valid = 1;
	my $operation = Opts::get_option('operation');

	if( defined $operation ){
		if (!exists($operations{$operation})) {
			Util::trace(0, "Invalid operation: '$operation'\n");
			Util::trace(0, " List of valid operations:\n");
			map {print "  $_\n"; } sort keys %operations;
			$valid = 0;
		}
	}
	return $valid;
}

BEGIN {
 $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;
 delete $ENV{http_proxy};
 delete $ENV{https_proxy};
}


__END__

=head1 NAME

power_vm.pl - Perform poweron, poweroff, reset, reboot,
shutdown and status operations on virtual machines.

=head1 SYNOPSIS

power_vm.pl --operation <poweron|poweroff|reset|reboot|shutdown|status> [options]

=head1 DESCRIPTION

This VI Perl command-line utility provides an interface for
seven common provisioning operations on one or more virtual
machines. However, this application only use five: 
powering on, powering off, resetting, rebooting, and
shutting down.

=head1 OPTIONS

=head2 OPERATION OPTIONS

=over

=item B<operation>

Operation to be performed. One of the following:

I<poweron> -- Power on one or more virtual machines.

I<poweroff> -- Power off one or more virtual machines.

I<reboot> -- Reboot one or more guests.

I<reset> -- Reset one or more virtual machines.

I<shutdown> -- Shutdown one or more guests.

I<status> -- Display power status one or more virtual machines.

=back

=head2 POWER OFF OPTIONS

=over

=item B<server>

IP address or fqdn of the VirtualCenter server's web service,
for example, --server vcenter.itap.purdue.edu

=item B<vmname>

Required. Name of the virtual machine on which the
operation is to be performed. Regexp is allowed.

=back

=head2 POWER ON OPTIONS

=over

=item B<vmname>

Required. Name of the virtual machine on which the
operation is to be performed. Regexp is allowed.

=back

=head2 REBOOT OPTIONS

=over

=item B<vmname>

Required. Name of the virtual machine on which
the operation is to be performed.

=back

=head2 SHUTDOWN OPTIONS

=over

=item B<vmname>

Required. Name of the virtual machine on which the
operation is to be performed.

=back

=head2 RESET OPTIONS

=over

=item B<vmname>

Required. Name of the virtual machine on which the
operation is to be performed.

=back

=head1 EXAMPLES

Power on a virtual machine

power_vm.pl --username user --password mypassword
--operation poweron --server vcenter.itap.purdue.edu --vmname ldvunxbuild01

power_vm.pl --username user --password mypassword
--operation poweron --server vcenter.itap.purdue.edu --vmname l[pdt]vunx

Power off a virtual machine

power_vm.pl --username user --password mypassword
--operation poweroff --server vcenter.itap.purdue.edu --vmname ldvunxbuild01

Reboot

power_vm.pl --username user --password mypassword
--operation reboot --server vcenter.itap.purdue.edu --vmname ldvunxbuild01

Shutdown

power_vm.pl --username user --password mypassword
--operation shutdown --server vcenter.itap.purdue.edu --vmname ldvunxbuild01

Reset

power_vm.pl --username user --password mypassword
--operation reset --server vcenter.itap.purdue.edu --vmname ldvunxbuild01

=head1 SUPPORTED PLATFORMS

All operations supported on ESX 3.0.1

All operations supported on VirtualCenter 2.0.1

