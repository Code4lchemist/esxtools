#!/usr/bin/perl
 
# The simpleclient.pl script outputs a list of all the entities of the specified managed-entity
# type (ClusterComputeResource, ComputeResource, Datacenter, Datastore, Folder, HostSystem,
# Network, ResourcePool, VirtualMachine, or VirtualService) found on the target vCenter Server or
# ESX system. Script users must provide logon credentials and the managed entity type. The script
# leverages the Util::trace() subroutine to display the found entities of the specified type.
 
use strict;
use warnings;
use VMware::VIRuntime;
 
# Defining attributes for a required option named 'entity' that
# accepts a string.
#
my %opts = (
        'vmname' => {
        	type => "=s",
	        help => "The name of the virtual machine to apply operation",
        	required => 1,
        },
	'operation' => {
		type => "=s",
                help => "[query|update]",
                required => 0,
                default => "query",
	},
	'params' => {
		type => "=s",
                help => "param1_name=param1_value|param2_name=param2_value",
                required => 0,
	},
	'header' => {
		type => "=s",
                help => "Display header [Yes|No]",
                required => 0,
                default => "Yes",
	},	
);

# validate options, and connect to the server
Opts::add_options(%opts);
 
# Parse all connection options (both built-in and custom), and then
# connect to the server
Opts::parse();
Opts::validate();
Util::connect();
 
# Obtain all inventory objects of the specified type
my $vmname = Opts::get_option('vmname');
my $operation = Opts::get_option('operation');
my $params = Opts::get_option('params');
my $header = Opts::get_option('header');

my $vm_views = Vim::find_entity_views(
			view_type => 'VirtualMachine', 
			properties => ['name', 'availableField', 'value'],
			filter => {'name' => qr/$vmname/});
			
if ($vm_views && @{$vm_views}) {
	if ( uc($header) eq 'YES' ){	
		print "\n++++++++++++++++++++++++++++++++++++++++\n";
		print " Virtual Machines - Custom Information\n";
		print "++++++++++++++++++++++++++++++++++++++++\n\n";
	}
	
	if ($operation eq 'query') {	
		foreach my $vm_view (@{$vm_views}) {
			&displayInfo($vm_view);
		}	
	} elsif ($operation eq 'update') {
		unless($params){
			Util::disconnect();
			print "Error: Operation \"update\" requires --params parameter!\n";
			exit 1;
		}
		foreach my $vm_view (@{$vm_views}) {				
			&updateInfo($vm_view, $params);
		}
	} else {
		print "Error: Invalid operation!\n";
	}
	
} else {
	Util::disconnect();
        print "Unable to find any VM name that matches the \"$vmname\" regexpr!\n";
        exit 0;
}

# Functions
sub displayInfo () {

	my ($vm_view) = @_;
	
	my $available_fields = $vm_view->availableField; 
	my %field_values = map {$_->{'key'} => $_->{'value'}} @{$vm_view->value || []};
	
	my ($vm_name) = ($vm_view->name =~ /^([^ ]*)/);
		
	my $vm_info = $vm_name . " | "; 

	foreach my $available_field (@$available_fields) {
		my $field_name = $available_field->name;
		my $field_key = $available_field->key;
		my $field_value = $field_values{$field_key} ? $field_values{$field_key}: "N/A";
		
		$vm_info = $vm_info . $field_name . "=" . $field_value . "|";
		
	}	
	Util::trace(0, "$vm_info\n");
}

sub updateInfo () {

	my ($vm_view, $params) = @_;
	
	
	my $custom_fields_mgr = Vim::get_view(mo_ref => Vim::get_service_content->customFieldsManager);
	my $available_fields = $vm_view->availableField;
	
	my %params_map =  map { (split /\=/, $_)[0] => (split /\=/, $_)[1] } split /\|/, $params ||
		die("Error: I couldn't read the new parameters. Check the format!");
	
	print "==> Updating Custome parameters on [ " . uc($vm_view->name) . " ]\n";

	while ( my ($param_name, $param_value) = each (%params_map)){
		print "\tSearching Field Name = \"" . $param_name . "\" ";
		my ($field) = grep { $_->{'name'} =~ /$param_name/ } @{$available_fields || []};
		
		if ($field) {
			print "<Field Found \"". $field->{'name'} ."\">.\n";
			print "\t\tUpdating Field Value to \"" . $param_value . "\""; 
			$custom_fields_mgr->SetField(entity => $vm_view, key => $field->{'key'}, value => $param_value);		
			print ". <Complete>\n";
		}else{
			print "\<Field Not Found \"" . $param_name . "\">.\n";
		}
	}
	
	print "\n";
	
}

# Disconnect from the server
Util::disconnect();

# Disable SSL hostname verification for vCenter self-signed certificate
BEGIN {
 $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;
 delete $ENV{http_proxy};
 delete $ENV{https_proxy};
}
