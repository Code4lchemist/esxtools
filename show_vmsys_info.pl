#!/usr/bin/perl -w

use strict;
use warnings;

use Net::IPv4Addr qw( :all );
use JSON;
use XML::Dumper;
use YAML;
use Data::Dumper;

use VMware::VIRuntime;
use VMware::VILib;

my %opts = (
        'vmname' => {
        		type => "=s",
			help => "The name of the virtual machine to apply operation",
        		required => 1,
        },
        'format' => {
	        	type => "=s",
        		help => "Display format (db,json,tree,xml)",
	        	required => 0,
			default => "tree",
        },
        'section' => {
	        	type => "=s",
        		help => "Display sections:\n" .
			"\ta = All\n" .
			"\tv = VMtools\n" .
			"\td = Description\n" .
			"\th = Hardware\n" .
			"\ts = Storage\n" .
			"\tp = Snapshots\n" .
			"\tn = Network\n" .
			"\tExample: show_vmsys_tools --section dh",
	        	required => 0,
			default => "a",
        },
        'showconsole' => {
        		type => "=s",
			help => "Generate a http link to HTML5 VMConsole\n" .
			"\tFind VMConsole link in the Hardware section",
        		required => 0,
			default => "false",
        },
);

# validate options, and connect to the server
Opts::add_options(%opts);

# validate options, and connect to the server
Opts::parse();
Opts::validate();
Util::connect();

my $apiVer  = Vim::get_service_content()->about->version;

my $server  = Opts::get_option('server');
my $vmname  = Opts::get_option('vmname');
my $format  = Opts::get_option('format');
my $section = Opts::get_option('section');
my $showConsole = Opts::get_option('showconsole');
#my @extFormats = qw(json xml yaml hash);
my $extFormats = "json xml yaml hash";
my $print   = !((lc($format) =~ $extFormats) || (lc($format) eq "db")) ? 1 : 0;
my %vmInfo  = ( vm1 => { "vmName" => undef, "vmSpecs" => undef });
my $vmCnt   = 0;

# Default ports and network configuration
my $consolePort = 7331;
my $sconsolePort = 7343;
my $port = 443;
my $vcenter_fqdn;

my $vm_views = Vim::find_entity_views(view_type => 'VirtualMachine',
				      properties => ['name', 'guest', 'config', 'availableField', 
					             'value', 'runtime', 'snapshot'], 
				      filter =>{ 'name' => qr/$vmname/});

if ($vm_views) {
	foreach my $vm_view (@$vm_views) {
		$vmCnt++;
		my $vm_name = $vm_view->name;
		print "VM [ " . lc($vm_name) . " ]\n" if $print;

		$vmInfo{'vm'. $vmCnt}{'vmName'} = $vm_name if lc($format) ;

		if ( (lc($section) =~ "v") || (lc($section) =~ "a") ) {
			# Display VMTools Info
			&vmtoolsInfo($vm_view);
			print "  |\n" if $print;
		}
		if ( (lc($section) =~ "d") || (lc($section) =~ "a") ) {
			# Display Custome Fields Info
			&vmCustomeFieldInfo($vm_view);
			print "  |\n" if $print;
		}
		if ( (lc($section) =~ "h") || (lc($section) =~ "a") || ($showConsole eq "true")) {
			# Display Hardware Info
			&vmHardwareInfo($vm_view);
			print "  |\n" if $print;
		}
		if ( (lc($section) =~ "s") || (lc($section) =~ "a") ) {
			# Display Storage Info
			&vmStorageInfo($vm_view);
			print "  |\n" if $print;
		}
		if ( (lc($section) =~ "p") || (lc($section) =~ "a") ) {
			# Display SnapShot Info
			&vmSnapShotInfo($vm_view);
			print "  |\n" if $print;
		}
		if ( (lc($section) =~ "n") || (lc($section) =~ "a") ) {
			# Display Network Information
			&vnicInfo($vm_view);
			print "  |\n" if $print;
		}

		print "\n" if lc($format) =~ /db tree/;
	}

	# Print output in different formats (Serialization)
	if (lc($format) eq "json") {
		my $output = encode_json \%vmInfo;
		print $output;
	}
	if (lc($format) eq "xml") {
	        my $output = XML::Dumper::pl2xml(\%vmInfo);
		print $output;
	}
	if (lc($format) eq "yaml") {
		my $output = Dump(\%vmInfo);
		print $output;
	}
	if (lc($format) eq "hash") {
		my $output = Dumper(\%vmInfo);
		print $output;
	}

	if (($showConsole eq "true") && (@{$vm_views} > 0)) {
		print "\n<< Note >> \n\tThis session will remain open for 60s to allow access to VMConsoles.\n";
		print "\tThe link to the HTML5 VMConsole is diplayed in the Hardware Section.\n";
		sleep(60);
	}
}

sub vmtoolsInfo() { 
	my ($vm_view) = @_;
	my $prefix = "VMT|";

	my $guestOS   = defined $vm_view->config->guestFullName ? $vm_view->config->guestFullName : "Unknown";
	my $syncTimeH = defined $vm_view->config->tools->syncTimeWithHost ? $vm_view->config->tools->syncTimeWithHost : 0;
	my $syncTime = $syncTimeH ? "True" : "False";
	my $toolStat = defined $vm_view->guest->toolsStatus ? $vm_view->guest->toolsStatus->val : "Null";
	my $toolVer  = defined $vm_view->guest->toolsVersionStatus ? $vm_view->guest->toolsVersionStatus : "Null";
	my $toolVer2 = defined $vm_view->guest->toolsVersionStatus2 ? $vm_view->guest->toolsVersionStatus2 : "Null";

	if ($print) {
		print "  +VMTOOLS\n";
		print "     --> Guest OS        = " . $guestOS  . "\n";
		print "     --> SyncTime Host   = " . $syncTime . "\n";
		print "     --> vmTool Status   = " . $toolStat . "\n";
		print "     --> vmTool Version  = " . $toolVer  . "\n";
		print "     --> vmTool Version2 = " . $toolVer2 . "\n";
	}

	if (lc($format) eq "db" ) { 
		print $prefix . $vm_view->name . "|" . $guestOS 
		              . "|" . $syncTime . "|" . $toolStat 
			      . "|" . $toolVer . "|" . $toolVer2 . "\n";
	}

	if (lc($format) =~ $extFormats) {
		$vmInfo{'vm' . $vmCnt}{'vmSpecs'}{vmTools} = { 
			                       'guestOS'  => $guestOS, 'syncTime' => $syncTime ,
			                       'toolStat' => $toolStat, 'tooVer'  => $toolVer, 
			                       'toolVer2' => $toolVer2 };
	}

}

sub vmCustomeFieldInfo() {
	my ($vm_view) = @_;
	my $prefix = "DES|";
	my $available_fields = $vm_view->availableField;
	my %field_values = map {$_->{'key'} => $_->{'value'}} @{$vm_view->value || []};

	print "  +DESCRIPTION\n" if $print;
	print $prefix . $vm_view->name if (lc($format) eq "db" );
	foreach my $available_field (@$available_fields) {
		my $field_name = $available_field->name;
		my $field_key = $available_field->key;
		my $field_value = $field_values{$field_key} ? $field_values{$field_key}: "N/A";

		print "     --> " . $field_name . " = " . $field_value . "\n" if $print;
		if (lc($format) eq "db" ) { 
			print "|" . $field_name . "=" . $field_value;
		}
	        if (lc($format) =~ $extFormats ) {
		        $vmInfo{'vm'.$vmCnt}{'vmSpecs'}{vmCustomeField}{$field_name} = $field_value;
	        }
	}
	print "\n" if (lc($format) eq "db");

	my $vm_annotation = "N/A";
	if (defined $vm_view->config->annotation) {
		my $vm_annotation = $vm_view->config->annotation  =~ /\w/ ? $vm_view->config->annotation : "N/A";
	}

	print "     --> Annotations = " . $vm_annotation  . "\n" if $print;

	if (lc($format) =~ $extFormats ) {
	        $vmInfo{'vm'.$vmCnt}{'vmSpecs'}{vmCustomeField}{Annotations} = $vm_annotation;
	}

}

sub genHTML5VMConsole() {
	# Copyright (c) 2009-2013 William Lam All rights reserved.
	my ($vm_view) = @_;
	my $mo_ref_id = $vm_view->{'mo_ref'}->value;
	# This section generates the html5 console session uri, which allows you to open
	# a console in any browser of your predilection.

	# Retrieve vCenter Server FQDN
	my $settingsMgr = Vim::get_view(mo_ref => Vim::get_service_content()->setting);
	my $settings = $settingsMgr->setting;

	foreach my $setting (@$settings) {
		if($setting->key eq 'VirtualCenter.FQDN') {
			$vcenter_fqdn = $setting->value;
			last;
		}
	}

	# Retrieve session ticket
	my $sessionMgr = Vim::get_view(mo_ref => Vim::get_service_content()->sessionManager);
	my $session = $sessionMgr->AcquireCloneTicket();

	# vCenter Server SHA1 SSL Thumbprint
	my $vcenterSSLThumbprint = `openssl s_client -connect $server:$port < /dev/null 2>/dev/null | openssl x509 -fingerprint -noout -in /dev/stdin | awk -F = '{print \$2}'`;
	$vcenterSSLThumbprint =~ s/^\s+//;
	$vcenterSSLThumbprint =~ s/\s+$//;

	print "Server = $server \n";

	$server = `ping -nc 1 -w 2 $server|grep -co 172.30.172.22 2>/dev/null` > 0 ? 'wppadmvi01.itap.purdue.edu' : $server;
	my $uriHTML5VMConsole = "http://" . $server . ":" . $consolePort . 
	                       "/console/?vmId=" . $mo_ref_id . 
			       "&vmName=" . $vmname . 
			       "&host=" . $vcenter_fqdn . 
			       "&sessionTicket=" . $session . 
			       "&thumbprint=" . $vcenterSSLThumbprint;
	return $uriHTML5VMConsole;
}

sub vmHardwareInfo() { 
	my ($vm_view) = @_;

	my $prefix = "HWR|";
	my $pwrStatus = $vm_view->runtime->powerState->val;
	my $nCPUs = $vm_view->config->hardware->numCPU;
	my $nCoresPerSocket = $vm_view->config->hardware->numCoresPerSocket;
	my $memory  = scaleIt($vm_view->config->hardware->memoryMB * 1048576); 
	my $vmVersion = $vm_view->config->version;
	my $mo_ref_id = $vm_view->{'mo_ref'}->value;

	if ($print) {
		print "  +HARDWARE\n";
		print "     --> MO_REF_ID    = " . $mo_ref_id  . "\n";
		print "     --> No CPUs      = " . $nCPUs  . "\n";
		print "     --> Cores/Socket = " . $nCoresPerSocket  . "\n";
		print "     --> Memory Size  = " . $memory . "\n";
		print "     --> VM Version   = " . $vmVersion . "\n";
		print "     --> Power Status = " . $pwrStatus  . "\n";
		print "     --> VMConsole    = " . &genHTML5VMConsole($vm_view) . "\n" if $showConsole eq "true";
	}

	if (lc($format) eq "db" ) { 
		print $prefix . $vm_view->name . "|" . $mo_ref_id . "|" . $nCPUs . "|" 
		              . $nCoresPerSocket . "|" . $memory . "|" 
			      . $vmVersion . "|" . $pwrStatus . "\n";
	}

	if (lc($format) =~ $extFormats) {
		$vmInfo{'vm' . $vmCnt}{'vmSpecs'}{vmHardware} = {
			                       'mo_Ref_Id'  => $mo_ref_id,
			                       'nCPUs'  => $nCPUs, 'nCoreSockets' => $nCoresPerSocket,
			                       'memory' => $memory, 'hwrVersion'  => $vmVersion,
			                       'powerStatus' => $pwrStatus
				       };
	}


}

sub vmStorageInfo() {
	my ($vm_view) = @_;
	my $devices = $vm_view->config->hardware->device;
	my $prefix = "STO|";

	print "  +STORAGE\n" if $print;
	$vmInfo{'vm' . $vmCnt}{'vmSpecs'}{vmStorage} = {} if (lc($format) =~ $extFormats);

	foreach my $device (@$devices) {
		if ($device->deviceInfo->label =~ /Hard disk/){
			my $diskId    = $1 if $device->deviceInfo->label =~ /Hard disk ([0-9]+)/;
			my $lunUuid;
			my $diskType = "RDM";
			eval {$lunUuid = $device->backing->lunUuid;};
			unless (defined $lunUuid) { 
				$diskType = "VirtualDisk";
				$lunUuid = "Null";
			}
			my $diskSize  = scaleIt($device->capacityInKB * 1024);
			my $fileName  = $device->backing->fileName;
			my $datastore = Vim::get_view(mo_ref => $device->backing->datastore);
			my $dtstName = $datastore->name;
			my $dtstSize = scaleIt($datastore->summary->capacity);
			my $dtstFree = scaleIt($datastore->summary->freeSpace);
			my $dtstUnco = scaleIt($datastore->summary->uncommitted);

			if ( $print ) {
				print "     -Hard Disk [" . $diskId . "]\n";
				print "       --> Disk Size = " . $diskSize . "\n";
				print "       --> Disk Type = " . $diskType . "\n";
				print "       --> LUN Uuid  = " . $lunUuid . "\n" ;
				print "       --> File Name = " . $fileName . "\n";
				print "       --> Datastore Name = " . $dtstName . "\n";
				print "       ----> Capacity    = " . $dtstSize  . "\n";
				print "       ----> Free Space  = " . $dtstFree  . "\n";
				print "       ----> Uncommitted = " . $dtstUnco  . "\n";
			}

			if (lc($format) eq "db" ) {
				print $prefix . $vm_view->name . "|" . $diskId . "|"
				              . $diskSize . "|" . $diskType . "|" . $fileName . "|"
				              . $lunUuid  . "|" . $dtstName . "|" . $dtstSize . "|"
				              . $dtstFree . "|" . $dtstUnco . "\n";
			}
			if (lc($format) =~ $extFormats) {
				$vmInfo{'vm' . $vmCnt}{'vmSpecs'}{vmStorage}{'vmDisk' . $diskId} = {
							       'diskSize'  => $diskSize, 
							       'diskType' => $diskType,
							       'lunUuid' => $lunUuid, 
							       'dtstoName'  => $dtstName,
							       'dtstoSize' => $dtstSize,
							       'dtstoFree' => $dtstFree,
							       'dtstoUnco' => $dtstUnco
						       };
			}
		}
	}
}

sub vmSnapShotInfo() {
	my ($vm_view) = @_;
	my $prefix = "SNP|";

	print "  +SNAPSHOTS\n" if $print;
	$vmInfo{'vm' . $vmCnt}{'vmSpecs'}{vmSnapshots} = {} if (lc($format) =~ $extFormats);
	if ( defined $vm_view->snapshot ) {
	        my $snapshots = $vm_view->snapshot->rootSnapshotList;
		my $noSnapshots = @{$snapshots};
		for ( my $i = 0; $i < $noSnapshots; $i++ ) {
			my $snap = $snapshots->[$i];
			my $snapId = $snap->id;
			my $snapName = $snap->name;
			my $snapDesc = $snap->description;
			my $snapCTime = $snap->createTime;

			if ( $print ) {
				print "     -SnapShot [" . $snapId . "]\n";
				print "       --> Name    = " . $snapName . "\n";
				print "       --> Description     = " . $snapDesc . "\n";
				print "       --> Created Time    = " . $snapCTime . "\n";
			}
			if (lc($format) eq "db") {
				print $prefix . $vm_view->name . "|" 
				              . $snapId   . "|" . $snapName . "|" 
					      . $snapDesc . "|" . $snapCTime . "\n";
			}
			if (lc($format) =~ $extFormats) {
				$vmInfo{'vm' . $vmCnt}{'vmSpecs'}{vmSnapshots}{'vmSnap' . $snapId} = {
							       'snapName'  => $snapName, 
							       'snapDesc' => $snapDesc,
							       'snapCTime' => $snapCTime 
						       };
			}
		}

	} else {
		if ( $print ) {
			print "     -SnapShot [ N/A ]\n";
		}
		if (lc($format) eq "db") {
			print $prefix . $vm_view->name . "|N/A\n";
		}
		if (lc($format) =~ $extFormats) {
			$vmInfo{'vm' . $vmCnt}{'vmSpecs'}{vmSnapshots} = "N/A";
		}
	}

}

sub vnicInfo() {
	my ($vm_view) = @_;
	my $prefix_nic = "NIC|";
	my $prefix_ip  = "IPA|";

	print "  +NETWORK\n" if $print;
	$vmInfo{'vm' . $vmCnt}{'vmSpecs'}{vmNetwork} = {} if (lc($format) =~ $extFormats);
	if ( defined $vm_view->guest->net ) {
		&guestNicInfo($vm_view);
	} else {
		&hwrNicInfo($vm_view);
	}

}

sub hwrNicInfo() {
	my ($vm_view) = @_;
	my $prefix_nic = "NIC|";
	my $prefix_ip  = "IPA|";
	my $devices = $vm_view->config->hardware->device;

	foreach my $device (@$devices) {
		if ($device->deviceInfo->label =~ /Network adapter/){
			my $nicId = $1 if $device->deviceInfo->label =~ /Network adapter ([0-9]+)/;
			$nicId = $nicId > 0 ? $nicId - 1 : 0;
			my $macAddress = defined $device->macAddress ? $device->macAddress : "N/A";
			my $status     = defined $device->connectable->connected ? "connected" : "disconnected";
			my $vlanId     = "N/A";
			if (defined $device->backing->port->portgroupKey){
				my $portGroup  = $device->backing->port->portgroupKey;
				my $dvNetwork  = Vim::find_entity_view( view_type  => 'DistributedVirtualPortgroup',
								        properties => ['name', 'key'], 
								        filter => {'key' => $portGroup});
				$vlanId = $dvNetwork->name =~ /-VLAN([0-9]+)/ ? $1 : $vlanId;
			}

			if ( $print ) {
				print "     -vNIC [" . $nicId . "]\n";
				print "       --> MAC Addr    = " . $macAddress . "\n";
				print "       --> VLAN Id     = " . $vlanId     . "\n";
				print "       --> PHY Link    = " . $status     . "\n";
			}
			if (lc($format) eq "db") {
				print $prefix_nic . $vm_view->name . "|" . $nicId 
				                  . "|" . $macAddress . "|" . $vlanId 
						  . "|" . $status . "\n";
			}
			if (lc($format) =~ $extFormats) {
				$vmInfo{'vm' . $vmCnt}{'vmSpecs'}{vmNetwork}{'vmNic' . $nicId} = {
							       'macAddress'  => $macAddress,
							       'vlanId' => $vlanId,
							       'status' => $status
						       };
			}
		}
	}
}

sub guestNicInfo() {
	my ($vm_view) = @_;
	my $prefix_nic = "NIC|";
	my $prefix_ip  = "IPA|";

	if ( defined $vm_view->guest->net ) {
		my $noGuestNics = @{$vm_view->guest->net};
		for ( my $nicId = 0; $nicId < $noGuestNics; $nicId++ ) {
			my $guestNic = $vm_view->guest->net->[$nicId];
			my $macAddress = $guestNic->macAddress;
			my $vlanId = 'N/A';
			if (defined $guestNic->network) {
				$vlanId = $guestNic->network =~ /-VLAN([0-9]+)/ ? $1 : 'N/A';
			}
			my $status = $guestNic->connected ? "connected" : "disconnected";

			if ( $print ) {
				print "     -vNIC [" . $nicId . "]\n";
				print "       --> MAC Addr    = " . $macAddress . "\n" if $macAddress;
				print "       --> VLAN Id     = " . $vlanId     . "\n";
				print "       --> PHY Link    = " . $status . "\n";
			}
			if (lc($format) eq "db") {
				print $prefix_nic . $vm_view->name . "|" . $nicId . "|" 
				                  . $macAddress . "|" . $vlanId . "|" 
						  . $status . "\n";
			}
			if (lc($format) =~ $extFormats) {
				$vmInfo{'vm' . $vmCnt}{'vmSpecs'}{vmNetwork}{'vmNic' . $nicId} = {
							       'macAddress'  => $macAddress,
							       'vlanId' => $vlanId,
							       'status' => $status
						       };
			}
			#Display IP information if it is available
			if ( defined $guestNic->ipAddress ) {
				my $nIPs = @{$guestNic->ipAddress};
				for (my $j = 0; $j < $nIPs; $j++) {
					if ( my $ip = ipv4_chkip($guestNic->ipAddress->[$j])) {
						my $hostname = `host $ip`;
						$hostname = $hostname =~ /pointer (.*)\.$/ ? $1 : "Unknown";
						my $cidr = "Null";
						my $mask = "Null";
						my $network = "Null";
						if ( defined $guestNic->ipConfig ) {
							$cidr = $guestNic->ipConfig->ipAddress->[$j]->prefixLength;
							$mask = ipv4_cidr2msk( "$cidr" );
							$network = ipv4_network( "$ip/$cidr" );
						}
						if ( $print )  {
							print "       ----> Hostname [$j] = " . $hostname . "\n" if $hostname;
							print "       ----> IP Addr  [$j] = " . $ip   . "\n" 	 if $ip;
							print "       ----> NETMASK  [$j] = " . $mask . "\n" 	 if $mask;
							print "       ----> NETWORK  [$j] = " . $network . "\n"  if $mask;
						}
						if (lc($format) eq "db") {
							print $prefix_ip . $vm_view->name . "|" . $nicId . "|" 
							                 . $j  . "|" . $hostname . "|" 
							                 . $ip . "|" . $mask . "|" 
									 . $network . "|" . $vlanId . "\n";
						}
						if (lc($format) =~ $extFormats) {
							$vmInfo{'vm' . $vmCnt}{'vmSpecs'}{vmNetwork}{'vmNic' . $nicId}{'IP'.$j} = {
										       'hostname'  => $hostname,
										       'ipAddress' => $ip,
										       'netMask' => $mask,
										       'network' => $network,
									       };
						}
					}
				}
			}


		}

	}
}

sub scaleIt {
	my ($size, $n) = (shift, 0);
	++$n and $size /= 1024 until $size < 1024;
	return sprintf "%.2f %s", $size, (qw[ bytes KB MB GB TB ] )[ $n ];
}

Util::disconnect();

BEGIN {
 $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;
 delete $ENV{http_proxy};
 delete $ENV{https_proxy};
}

__END__

=head1 NAME

show_vmsys_info.pl - Display virtual machine information and open console

=head1 SYNOPSIS

show_vmsys_info.pl [options]

=head1 DESCRIPTION

This VI Perl command-line utility connects to the vcenter and 
display virtual machine(s) configuration

=head1 OPTIONS

=over

=item B<vmname>

Required. The name of the virtual machine(s). It will be used to select the virtual machine.

=item B<section>

Optional. The virtual machine configuration is separated into sections 
that can be displayed seperately. 

=item B<format>

Optional. This report can be displayed in different formats: tree, db, json, tree, xml.

=item B<showconsole>

Optional. This option generates a http link to a HTML5 VMConsole. 
The VMConsole link is display in the hardware section.

=back

=head1 EXAMPLES

Display all the attributes the virtual machine maVM:

  show_vmsys_info.pl --vmname maVM.example.lcl 

Display all the attributes of multiple machines using regexp 

  show_vmsys_info.pl --vmname "m[bc]VM.example.lcl"

Display all the attributes based on a specific section of the virtual machine

  show_vmsys_info.pl --vmname maVM --section h

Generate a VMconsole http link [Note the link is show in the hardware section]
  show_vmsys_info.pl --vmname maVM --showconsole true 
  
Sample Output:

+HARDWARE
     --> MO_REF_ID    = vm-87911
     --> No CPUs      = 1
     --> Cores/Socket = 1
     --> Memory Size  = 2.00 GB
     --> VM Version   = vmx-08
     --> Power Status = poweredOn
     --> VMConsole    = http://vcenter.example.lcl:7331/console/?vmId=vm-87911&vmName=maVM&host=hos1.example.lcl&sessionTicket=cst-VCT-52593145-605d-51af-ef6d-c01c38e0b8e4--tp-7A-90-CF-7D-59-7E-A8-1C-B9-F2-4E-59-71-2D-F1-D0-19-08-0A-17&thumbprint=7A:90:CF:7D:59:7E:A8:1C:B9:F2:4E:59:71:2D:F1:D0:19:08:0A:17
  |

=head1 SUPPORTED PLATFORMS

All operations work with VMWare VirtualCenter 2+ and VMWare ESX 3+
