#!/usr/bin/perl -w


use strict;
use warnings;
use VMware::VIRuntime;
use VMware::VILib;

my %opts = (
        'vmname' => {
        	type => "=s",
	        help => "The name of the virtual machine to apply operation",
        	required => 1,
        },
        'vnic' => {
	        type => "=s",
        	help => "vNIC Adapter # (e.g. 1,2,3,etc or '?' to display all)",
	        required => 0,
		default => "?",	        
        },
	'operation' => {
		type => "=s",
                help => "[query|add]",
                required => 0,
		default => "query",
	},
        'nictype' => {
	        type => "=s",
        	help => "pcnet|vmxnet2|vmxnet3|e1000|e1000e",
	        required => 0,
		default => "vmxnet3",
        },
        'vlan' => {
	        type => "=s",
        	help => "The VLAN ID (e.g. 3794)",
	        required => 0,
        },
);
# validate options, and connect to the server
Opts::add_options(%opts);

# validate options, and connect to the server
Opts::parse();
Opts::validate();
Util::connect();

my $apiVer = Vim::get_service_content()->about->version;

my $vnic_device;
my $vmname = Opts::get_option('vmname');
my $vnic = Opts::get_option('vnic');
my $operation = Opts::get_option('operation');
my $nictype = Opts::get_option('nictype');
my $mac = Opts::get_option('mac');
my $network = Opts::get_option('vlan');

my $vm_view = Vim::find_entity_view(view_type => 'VirtualMachine', 
				    filter =>{ 'name' => qr/$vmname/});

if ($vm_view) {

	my $devices = $vm_view->config->hardware->device;
	
	print "\n++++++++++++++++++++++++++++++++++++++++\n";
	print "VM Name: " . uc($vm_view->name) . "\n";
	print "++++++++++++++++++++++++++++++++++++++++\n\n";
	
	#get information about the current vNIC
        
        if ($operation eq 'query') {	
        	unless($vnic) {
			Util::disconnect();
			print "Error: operation \"query\" requires --vnic parameter!\n";
			exit 1;
		}
		
		foreach my $device (@$devices) {
			if ($device->deviceInfo->label =~ /Network adapter $vnic/){
				$vnic_device = $device;
				&query($vnic_device,$vm_view);
			}
		}
		
		unless (defined $vnic_device) {
			print "Unable to find vNIC $vnic\n";
		}
	} elsif ($operation eq 'add') {
		
		unless($nictype) {
                        Util::disconnect();
                        print "Error: operation \"add\" requires --nictype parameter!\n";
                        exit 1;
                }
                
                unless($network) {
                        Util::disconnect();
                        print "Error: operation \"add\" requires --vlan parameter!\n";
                        exit 1;
                }
				
		&addnic($vm_view,$nictype,$network);
	
	} else {
			print "Error: Invalid operation!\n";
	}
	
} else {
	Util::disconnect();
        print "Unable to locate $vmname!\n";
        exit 0;
}

Util::disconnect();

# Functions
sub query() {
	my ($vnic_device,$vm_view) = @_;

	my $currMac = $vnic_device->macAddress;
	my $portGroup = $vnic_device->backing->port->portgroupKey;
	my $dvNetwork = Vim::find_entity_view(view_type => 'DistributedVirtualPortgroup',
					       properties => ['name', 'key'], 
					       filter => {'key' => $portGroup});
	
	print "Device: " . $vnic_device->deviceInfo->label . "\n";
	print "----------------------------------------\n";
        print "NIC Type: " . ref($vnic_device) . "\n";
        print "MAC Address: " . $currMac . "\n";
	print "VLANID: ";
	
	if ($dvNetwork) { 
		print  ($dvNetwork->name =~ /.*-VLAN(.*)/);
		print "\n\n"; 
	}else {
		print "N/A \n\n";
	}
}

sub addnic() {
	my ($vm_view,$nic_type,$nic_network) = @_;
	
	my ($task_ref,$msg);

	if ($nic_type eq 'e1000e' && $vm_view->config->version ne "vmx-08") {
                Util::disconnect();
                print "\ne1000e vNIC is only supported on VMs that are Hardware Version 8\n";
                exit 1;
        }
	
	# Find dvportgroup
	my $dvportgroup_view = Vim::find_entity_view(
					view_type => 'DistributedVirtualPortgroup',
					filter => {'name' => qr/$nic_network/});
	
	# Retrieve dvportgroup key and DVS reference object to capture uuid
	my $port_group_key = $dvportgroup_view->key;
	
	# Retrieve DVS uuid
	my $dvs_entity_key = $dvportgroup_view->config->distributedVirtualSwitch;
	my $dvs_entity = Vim::get_view(mo_ref => $dvs_entity_key);
   	my $dvs_uuid = $dvs_entity->uuid;
	
	# New object that represents a connection between a DVPortgroup and a VM vNIC
   	my $dvs_port_connection = DistributedVirtualSwitchPortConnection->new(
                                       portgroupKey => $port_group_key,
                                       switchUuid => $dvs_uuid);
	
	# New object that defines backing for a vEth card that connects to a dvSwitch Port
	my $backing_info = VirtualEthernetCardDistributedVirtualPortBackingInfo->new(
                                       port => $dvs_port_connection);
                                       
	# New object which contains information about connectable virtual devices
   	my $vdev_connect_info = VirtualDeviceConnectInfo->new(
                                       startConnected => '1',
                                       allowGuestControl => '1',
                                       connected => '1');

	# Buiding the new Virtual Network Interface		
	my $newNetworkDevice;
	if ($nic_type eq 'e1000') {
        	$newNetworkDevice = VirtualE1000->new(key => -1, backing => $backing_info, addressType => 'assigned', connectable => $vdev_connect_info);
	} elsif($nic_type eq 'e1000e' && $apiVer eq "5.0.0") {
		$newNetworkDevice = VirtualE1000e->new(key => -1, backing => $backing_info, addressType => 'assigned', connectable => $vdev_connect_info);
        } elsif($nic_type eq 'pcnet') {
                $newNetworkDevice = VirtualPCNet32->new(key => -1, backing => $backing_info, addressType => 'assigned', connectable => $vdev_connect_info);
        } elsif($nic_type eq 'vmxnet2') {
                $newNetworkDevice = VirtualVmxnet2->new(key => -1, backing => $backing_info, addressType => 'assigned', connectable => $vdev_connect_info);
	} elsif($nic_type eq 'vmxnet3') {
		$newNetworkDevice = VirtualVmxnet3->new(key => -1, backing => $backing_info, addressType => 'assigned', connectable => $vdev_connect_info);
        } else {
                Util::disconnect();
                die "Please select a valid nic type!\n";
        }
	
	my $vm_dev_spec = VirtualDeviceConfigSpec->new(
				operation => VirtualDeviceConfigSpecOperation->new('add'),
				device => $newNetworkDevice);
        my $vmChangespec = VirtualMachineConfigSpec->new(deviceChange => [ $vm_dev_spec ] );
	
	eval{
        	print "Adding new $nic_type vNic to \"$vmname\"\n";
                $task_ref = $vm_view->ReconfigVM_Task(spec => $vmChangespec);
                $msg = "\tSuccessfully reconfigured \"$vmname\"";
                &getStatus($task_ref,$msg);
        };
        if($@) {
        	print "Error: " . $@ . "\n";
        }
}

sub getStatus {
        my ($taskRef,$message) = @_;

        my $task_view = Vim::get_view(mo_ref => $taskRef);
        my $taskinfo = $task_view->info->state->val;
        my $continue = 1;
        while ($continue) {
                my $info = $task_view->info;
                if ($info->state->val eq 'success') {
                        print $message,"\n";
                        $continue = 0;
                } elsif ($info->state->val eq 'error') {
                        my $soap_fault = SoapFault->new;
                        $soap_fault->name($info->error->fault);
                        $soap_fault->detail($info->error->fault);
                        $soap_fault->fault_string($info->error->localizedMessage);
                        die "$soap_fault\n";
                }
                sleep 5;
                $task_view->ViewBase::update_view_data();
        }
}

# Disable SSL hostname verification for vCenter self-signed certificate
BEGIN {
 $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;
 delete $ENV{http_proxy};
 delete $ENV{https_proxy};
}


