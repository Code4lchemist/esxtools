#! /bin/bash
if [ -z "${2}" ];then server=vcenter.itap.purdue.edu;else server=${2};fi
up_vmtool='/home/admin/maruiz/UNIX-Admin/bin/vmcmds/updateVMTools.pl'

if [ $# -ne 2 ]
then
    echo "This command only support two parameters"
    echo "dpVMTools.sh [vmname] [server]"
    echo "Example: dpVMTools.sh ldvopslog01 vcenter.itap.purdue.edu"
    exit 1
fi

vmname=${1}
echo ""
echo "-------------------------------------"
echo "Installing/Upgrading VMWare Tools -->  [ $vmname ]"
echo "-------------------------------------"

toolStatus=`${up_vmtool} --server=${server} --vmname=${vmname} --operation=query|awk -F"|" '{print $2, $3}'`
echo "VMWareTools [ $toolStatus ]"

if [[ $toolStatus =~ toolsOk.*Current ]]
then
    echo "--+ No further proccess is needed"
    exit 0
fi

${up_vmtool} --server=${server} --header=no --vmname=${vmname} --operation=update

sleep 5
if [[ $toolStatus =~ toolsNotInstalled || $toolStatus =~ toolsOk.*Unmanaged ]]
then 
    echo "--+ Running MSRC to install VMWareTools" 
    ssh -qto "StrictHostKeyChecking no" puppet "cd bin/install_vmtools && msrc -G ${vmname} op make install"
fi

echo "--+ Process Complete!" 
toolStatus=`${up_vmtool} --server=${server} --vmname=${vmname} --operation=query|awk -F"|" '{print $2, $3}'`
echo "VMWareTools [ $toolStatus ]"
