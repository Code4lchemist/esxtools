#!/usr/bin/perl
 
use strict;
use warnings;
use Term::ANSIColor;
use VMware::VIRuntime;
 
# Defining attributes for a required option named 'entity' that
# accepts a string.
#
my %opts = (
	'vmname' => {
        	type => "=s",
	        help => "The name of the virtual machine to apply operation",
        	required => 1,
        },
        'operation' => {
		type => "=s",
                help => "[query|update]",
                required => 0,
                default => "query",
	},
	'forced' => {
		type => "=s",
                help => "Don't ask and enforce [Yes|No]",
                required => 0,
		default => "No",
	},
	'header' => {
		type => "=s",
                help => "Display header [Yes|No]",
                required => 0,
	},
);
Opts::add_options(%opts);
 
# Parse all connection options (both built-in and custom), and then
# connect to the server
Opts::parse();
Opts::validate();
Util::connect();
 
my $apiVer = Vim::get_service_content()->about->version;

my $vmname = Opts::get_option('vmname');
my $operation = Opts::get_option('operation');
my $forced = Opts::get_option('forced');
my $header = Opts::get_option('header');

# Obtain all inventory objects of the specified type

my $vm_views = Vim::find_entity_views(
			view_type => 'VirtualMachine', 
			properties => ['name', 'guest', 'runtime', 'config'],
			filter => {'name' => qr/$vmname/});

if ($vm_views) {
        
        if ($operation eq 'query') {	

        	&query($vm_views);
		
	} elsif ($operation eq 'update') {
				
		&update_tool($vm_views);
	
	} else {
		print "Error: Invalid operation!\n";
	}
	
} else {
	Util::disconnect();
        print "Unable to locate $vmname!\n";
        exit 0;
}				   
 
 
# Disconnect from the server
Util::disconnect();

# Functions
sub getStatus {
        my ($taskRef,$message) = @_;

        my $task_view = Vim::get_view(mo_ref => $taskRef);
        my $taskinfo = $task_view->info->state->val;
        my $continue = 1;
        while ($continue) {
                my $info = $task_view->info;
                if ($info->state->val eq 'success') {
                        print $message,"\n";
                        $continue = 0;
                } elsif ($info->state->val eq 'error') {
                        my $soap_fault = SoapFault->new;
                        $soap_fault->name($info->error->fault);
                        $soap_fault->detail($info->error->fault);
                        $soap_fault->fault_string($info->error->localizedMessage);
                        die "$soap_fault\n";
                }
		printf ".";
                sleep 5;
                $task_view->ViewBase::update_view_data();
        }
}


sub update_tool() {

	my ($vm_views) = @_;

	my ($num_entities, $task_ref, $msg);
        my $curr_tasks = {};

	$num_entities = @{$vm_views};

        foreach my $vm_view (@$vm_views) {
                my $vm_name = $vm_view->name;
		my $vm_tool = $vm_view->guest->toolsStatus->val;
		my $vm_status = $vm_view->runtime->powerState->val;
		my $vm_tool_ver = $vm_view->guest->toolsVersionStatus; 
		my $vm_tool_ver2 = $vm_view->guest->toolsVersionStatus2; 

        	$header = defined ($header) ? $header : 'YES';

		if ( uc($header) eq 'YES' ){	
			print "\n++++++++++++++++++++++++++++++++++++++++\n";
			print "VM Name: " . uc($vm_view->name) . "\n";
			print "++++++++++++++++++++++++++++++++++++++++\n\n";
		}
        
                

        }
	if ( $num_entities == 1){
		foreach my $vm_view (@$vm_views) {
			my $vm_name = $vm_view->name;
			my $vm_tool = $vm_view->guest->toolsStatus->val;
			my $vm_status = $vm_view->runtime->powerState->val;
			my $vm_tool_ver = $vm_view->guest->toolsVersionStatus;

			$header = defined ($header) ? $header : 'YES';
		
			if ( uc($header) eq 'YES' ){	
				print "\n++++++++++++++++++++++++++++++++++++++++\n";
				print "VM Name: " . uc($vm_view->name) . "\n";
				print "++++++++++++++++++++++++++++++++++++++++\n\n";
			}

                        if ($vm_status eq 'poweredOn'){
				if ($vm_tool eq 'toolsOld'){
					print "Sending upgrade task to [ " .  $vm_name . " ] \n";
					eval{
						$task_ref = $vm_view->UpgradeTools_Task();
						$msg = "\tVMWare Tools were successfully upgraded in \"$vm_name\"";
						&getStatus($task_ref,$msg);
					};
					if($@) {
			        		print "\tError: " . $@ . "\n";
			        	}

				}elsif ($vm_tool =~ /toolsNot/ || $vm_tool_ver =~ /guestToolsUnmanaged/ || ($vm_tool eq 'toolsOk' && uc($forced) eq 'YES')){
					Util::trace(0, "Sending Mount VMwaretool task to [ $vm_name ] \n");
					eval{
						if (&mediaconnected($vm_view)){
							$task_ref = $vm_view->UnmountToolsInstaller();
   					        	sleep 5;	
						}
						$task_ref = $vm_view->MountToolsInstaller();
						$msg = "\tVMWare Tools were successfully mounted in \"$vm_name\"";
						#&getStatus($task_ref,$msg);
					};
					if($@) {
			        		print "\tError: " . $@ . "\n";
			        	}
				}elsif ($vm_tool eq 'toolsOk'){
					print "\tInfo: VMware Tools are already current [ ". $vm_tool . " ] \n"; 
					next;
				}else{
					print "\tWarning: Status Unknown [ " . $vm_tool . " ] \n";
					next;
				}

                        }else {
				print "\tError: The VM [ " . uc($vm_view->name) . " ] is not PoweredOn\n"; 
			}

		}
	
	}else {
		print "\tFailed to upgrade VMWare Tools: \n" .
		      "\tCause: More that one VM with same VM name \"" . $vmname . "\". \n\n";
		      &query($vm_views); 
	}

	
}

sub query () {

	my ($vm_views) = @_;

	foreach my $vm_view (@$vm_views) {

		my $stat_color = "red";
		my $vm_name = $vm_view->name;
		my $vm_tool = $vm_view->guest->toolsStatus->val;
		my $vm_tool_ver = $vm_view->guest->toolsVersionStatus;
		my $vm_tool_ver2 = $vm_view->guest->toolsVersionStatus2;
		my $vm_status = $vm_view->runtime->powerState->val;
                if ($vm_tool =~ /toolsOk/){
               	    $stat_color = "green"; 
		}elsif ($vm_tool =~ /toolsOld/){
                    $stat_color = "yellow";
                }
                &print("[$vm_name]","blue");
                print "|ToolStatus=";
                &print("$vm_tool", $stat_color);
                print "|Ver=$vm_tool_ver|ver2=$vm_tool_ver2|PwrStatus=$vm_status\n";
	
	}
}

sub mediaconnected () {
    my($vm_view) = @_;
    my $devices = $vm_view->config->hardware->device;
    my $status;
    foreach my $device (@$devices){
        if ($device->deviceInfo->label =~ /CD\/DVD drive/){
            $status = $device->connectable->connected;
            last;
        }
    }
    
    return $status;
}

sub print {
	my ($msg,$color) = @_;

	print color($color) . $msg . color("reset");
}

# Disable SSL hostname verification for vCenter self-signed certificate
BEGIN {
 $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;
 delete $ENV{http_proxy};
 delete $ENV{https_proxy};
}
