#! /bin/bash -x
while read hname
do
  dnsrecord=$(host ${hname}|grep address)
  if [ ! -z "${dnsrecord}" ]; then
     hostname=$(echo ${dnsrecord}|awk '{print $1}')
     ipaddress=$(echo ${dnsrecord}|awk '{print $4}')
     macaddress=$(sshpass -p 5nwndws5 ssh ${hname}-mgt racadm racdump|grep NIC.Integrated.1-3|sed 's/.*=[ ]\+//')
     echo "$macaddress $ipaddress $hostname 255.255.255.0 172.30.255.1 128.210.11.57,128.210.11.5"
  fi
done
