#!/usr/bin/perl -w
# Original Author: stumpr (http://communities.vmware.com/message/1265766#1265766)

use strict;
use warnings;

use VMware::VIRuntime;


Opts::parse();
Opts::validate();

Util::connect();

my ($datacenter_views, $vmFolder_view, $indent);

$indent = 0;

$datacenter_views = Vim::find_entity_views(
        view_type => 'Datacenter',
        properties => ["name", "networkFolder"],
);

foreach ( @{$datacenter_views} )
{
        print "Datacenter: " . $_->name . "\n";

        TraverseFolder($_->networkFolder, $indent);

}

sub TraverseFolder
{
        my ($entity_moref, $index) = @_;

        my ($num_entities, $entity_view, $child_view, $i, $mo);

        $index += 4;


        $entity_view = Vim::get_view(
                mo_ref => $entity_moref, properties => ['name', 'childEntity']
        );
        
        
	print " " x $index . "Network: " . $entity_view->name ;

        $num_entities = defined($entity_view->childEntity) ? @{$entity_view->childEntity} : 0;
        print " " x $index . "[" . $num_entities . "]\n";
        if ( $num_entities > 0 )
        {

                foreach $mo ( @{$entity_view->childEntity} )
                {
                        $child_view = Vim::get_view(
                                mo_ref => $mo, properties => ['name']
                        );

			if ( $child_view->name =~ /VLAN/) {
				$child_view = Vim::get_view(mo_ref => $mo, properties => ['name', 'key']);
				print " " x $index . $child_view->name . " " . $child_view->key . "\n" ;
			}else {
				print " " x $index . $child_view->name . ":\n" ;
			}
			
			

                        if ( $child_view->isa("Folder") )
                        {
                        	TraverseFolder($mo, $index);
                        }

                }
        }

}

# Disable SSL hostname verification for vCenter self-signed certificate
BEGIN {
 $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;
 delete $ENV{http_proxy};
 delete $ENV{https_proxy};
}

Util::disconnect();
