#!/usr/bin/perl -w

use strict;
use warnings;

use Data::Dumper;

use VMware::VIRuntime;
use VMware::VILib;

my %opts = (
        'vmname' => {
        		type => "=s",
			help => "The name of the virtual machine to apply operation",
        		required => 1,
        },
        'vdiskid' => {
	        	type => "=i",
        		help => "Disk ID",
	        	required => 0,
			default => 0,
        },
        'newsize' => {
	        	type => "=s",
        		help => "New size\n" .
			"\tM = MB\n" .
			"\tG = GB\n" .
			"\tT = TB\n" .
			"\tExample: adminVMSTO --vmname vm --vdiskid 1 --newsize 20G",
	        	required => 0,
        },
        'list' => {
        		type => "=s",
			help => "list the current storage configuration\n",
        		required => 0,
			default => "true",
        },
);

# validate options, and connect to the server
Opts::add_options(%opts);

# validate options, and connect to the server
Opts::parse();
Opts::validate();
Util::connect();

my $apiVer   = Vim::get_service_content()->about->version;

my $vmname   = Opts::get_option('vmname');
my $vdiskId  = Opts::get_option('vdiskid');
my $newsize  = Opts::get_option('newsize');
my $newsizeB = defined $newsize ? unscaleIt($newsize) : 0;
#Leave 100GB of free space on a DataStore
my $DTSTSIZE = 107374182400;
my $list     = Opts::get_option('list');
my $print    = $list =~ "false" ? 0 : 1;

my $vm_view = Vim::find_entity_view(view_type => 'VirtualMachine',
				      properties => ['name', 'config', 'snapshot'],
				      filter =>{ 'name' => qr/$vmname/});

if ($vm_view) {
	my $vm_name = $vm_view->name;
	print "VM [ " . lc($vm_name) . " ]\n" if $print;

	# Display Storage Info
	&vmStorageInfo($vm_view) if $print;
	if (defined($newsize) && $vdiskId == 0) {
		print "   Error: No valid vdiskid!";
		exit;
	}
	if (defined($newsize) && $newsizeB == 0) {
		print "   Error: Incorrect new storage size!";
		exit;
	}
	&vmStorageGrow($vm_view) if ($vdiskId * $newsizeB) > 0;
}

sub vmStorageInfo() {
	my ($vm_view) = @_;
	my $devices = $vm_view->config->hardware->device;
	my $regexp = $vdiskId > 0 ? $vdiskId : "([0-9]+)";

	print "  +STORAGE\n" if $print;

	foreach my $device (@$devices) {
		if ($device->deviceInfo->label =~ /Hard disk $regexp/) {
			my $diskId    = $1 if $device->deviceInfo->label =~ /Hard disk ([0-9]+)/;
			my $lunUuid;
			my $diskType = "RDM";
			eval {$lunUuid = $device->backing->lunUuid;};
			unless (defined $lunUuid) { 
				$diskType = "VirtualDisk";
				$lunUuid = "Null";
			}
			my $diskSize  = scaleIt($device->capacityInKB * 1024);
			my $fileName  = $device->backing->fileName;
			my $datastore = Vim::get_view(mo_ref => $device->backing->datastore);
			my $dtstName = $datastore->name;
			my $dtstSize = scaleIt($datastore->summary->capacity);
			my $dtstFree = scaleIt($datastore->summary->freeSpace);
			my $dtstUnco = scaleIt($datastore->summary->uncommitted);

			if ( $print ) {
				print "     -Hard Disk [" . $diskId . "]\n";
				print "       --> Disk Size = " . $diskSize . "\n";
				print "       --> Disk Type = " . $diskType . "\n";
				print "       --> LUN Uuid  = " . $lunUuid . "\n" ;
				print "       --> File Name = " . $fileName . "\n";
				print "       --> Datastore Name = " . $dtstName . "\n";
				print "       ----> Capacity    = " . $dtstSize  . "\n";
				print "       ----> Free Space  = " . $dtstFree  . "\n";
				print "       ----> Uncommitted = " . $dtstUnco  . "\n";
			}
		}
	}

	print "  |\n" if $print;
#	print Dumper( $devices);
}

sub vmStorageGrow() {
	my ($vm_view) = @_;
	my $devices = $vm_view->config->hardware->device;
	my $vdisk = undef;

	foreach my $device (@$devices) {
		if ($device->deviceInfo->label =~ /Hard disk $vdiskId/) {
			$vdisk = $device;
			last;
		}
	}

	if (defined $vdisk) {
		print "   ==> Modify " . $vdisk->deviceInfo->label . "\n";
		my $diskSize = $vdisk->capacityInKB * 1024;
		if ($diskSize > $newsizeB) {
			print "   Error: newsize is smaller than diskSize. \n";
		        exit;
		}
		my $datastore = Vim::get_view(mo_ref => $vdisk->backing->datastore);
		my $dtstSize = $datastore->summary->capacity;
		my $dtstFree = $datastore->summary->freeSpace;
		if (($newsizeB - $diskSize) > ($dtstFree - $DTSTSIZE)) {
			print "   Error: There is not enough space in Datastore. \n";
			exit;
		}
		if ( defined $vm_view->snapshot ) {
			print "   Error: There is at least a snapshot present. \n";
			print "          The VMDSK cannot be resized. \n";
			exit;
		}

	        my $specOp  = VirtualDeviceConfigSpecOperation->new('edit');
		my $chgDisk = VirtualDisk->new(capacityInKB  => $newsizeB/1024,
			                       backing 	     => $vdisk->backing,
					       deviceInfo    => $vdisk->deviceInfo,
					       controllerKey => $vdisk->controllerKey,
					       key           => $vdisk->key,
					       unitNumber    => $vdisk->unitNumber,
					       storageIOAllocation => $vdisk->storageIOAllocation,
				               );

		my $chgDiskCfgSpec  = VirtualDeviceConfigSpec->new(
					       operation => $specOp,
					       device    => $chgDisk );

		my $vMachineCfgSpec = VirtualMachineConfigSpec->new(
					       deviceChange  => [ $chgDiskCfgSpec ]);

		eval {
			print "   ==> Applying new VirtualDisk Configuration [". scaleIt($newsizeB) ."]\n";
			$vm_view->ReconfigVM(spec => $vMachineCfgSpec);
			Util::trace(0, "   Virtual machine " . $vm_view->name . " is reconfigured successfully!\n");
		};
		if ($@) {
			Util::trace(0, "   Error: Reconfiguration failed: ");
			if(ref($@) eq 'SoapFault') {
				if (ref($@->detail) eq 'TooManyDevices') {
					Util::trace(0, "\n   Number of virtual devices exceeds " . 
						       "the maximum for a given controller.\n");
				}
				elsif (ref($@->detail) eq 'InvalidDeviceSpec') {
					Util::trace(0, "   The Device configuration is not valid\n");
					Util::trace(0, "\n   Following is the detailed error: \n\n$@");
				}
				elsif ( ref($@->detail) eq 'FileAlreadyExists') {
					Util::trace(0, "\n   Operation failed because file already exists");
				}
				else {
					Util::trace(0, "\n   " . $@ . "\n");
				}
			} else {
				Util::trace(0, "\n   " . $@ . "\n");
			}
		}

	} else {
	       print "   Error: vdiskid not found!\n";
	       exit;
        }
}

sub scaleIt {
	my ($size, $n) = (shift, 0);
	++$n and $size /= 1024 until $size < 1024;
	return sprintf "%.2f %s", $size, (qw[ bytes KB MB GB TB ] )[ $n ];
}

sub unscaleIt {
	my ($number, $letter) = shift =~ /(\d+)(\w)/;
	$number = defined $number ? $number : 0;
	$letter = defined $letter ? $letter : 'Z';
	my %sizes = (K => 1024, M => 1024**2, G => 1024**3, T => 1024**4);
	return exists $sizes{$letter} && defined($number)? $number * $sizes{$letter}: 0;
}

Util::disconnect();

BEGIN {
 $ENV{PERL_LWP_SSL_VERIFY_HOSTNAME} = 0;
 delete $ENV{http_proxy};
 delete $ENV{https_proxy};
}
