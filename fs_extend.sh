#!/bin/bash

# Update storage size for both block device and physical volumen.
# It aslo resize the logical volumen and filesystem. - Miguel Ruiz

## Color Schemes
green='\e[0;32m'
yellow='\e[0;33m'
red='\e[0;31m'
endColor='\e[0m'


if [ $# -lt 2 ]; then
   echo "Usage: `basename $0` [filesystem] [lvextend resize options]"
   echo "Example 1: `basename $0` /usr/local -L +10G"
   echo "Example 2: `basename $0` /usr/local -l +100%FREE"
   exit 0
fi

fs_name="$1" && shift
lvextend_options="$@"

lvolume=$(df -hP ${fs_name} 2> /dev/null|awk '!/Filesystem/ {print $1}')
[ -z "$lvolume" ] && echo -e "${red}Error: filesystem ${fs_name} not found! ${endColor}" && exit 1

#Try to resize the logical volume and the filesystem
echo -e "${red}"
lvextend ${lvextend_options} -r ${lvolume} &&
echo -e "${green}Info: Logical volume ${lvolume} has been successfully resized! ${endColor}" &&
exit 0
echo -e "${endColor}"

echo -e -e "${yellow}==> Updating block-device's size and trying to resize again... ${endColor}\n"

dm_info=$(dmsetup info  -c --noheadings ${lvolume})
[ -z "$dm_info" ] && echo -e "${red}Error: Missing DM information for ${lvolume}! ${endColor}" && exit 1

lv_name=$(echo ${dm_info}|cut -d":" -f1)
dm_name=$(echo ${dm_info}|awk -F":" '{print "dm-" $3}')
lsdisks=$(ls /sys/block/${dm_name}/slaves/|sed -e '/^dm/!s/[0-9]$//'|sort -u)
[ -z "$lsdisks" ] && echo -e "${red}Error: PV not found for ${lv_name}! ${endColor}" && exit 1


### Updating filesystem size
for dev_name in ${lsdisks}
do
   if [ ! -z "${dev_name}" ]; then
      mpath_name=$(dmsetup info -c --noheadings -o name /dev/${dev_name} 2> /dev/null)
      if [[ "${mpath_name}" =~ "mpath" ]]; then
         echo -e "${yellow}==> Updating the size of multipath devices: ${endColor}"
         echo -e "echo 1 > /sys/block/${dev_name}/slaves/*/device/rescan"
         echo 1 > /sys/block/${dev_name}/slaves/*/device/rescan &&
         echo -e "${yellow}==> Updating multipath: ${endColor}" &&
         service multipathd status && multipathd -k"resize map ${mpath_name}"
      else
         echo -e "${yellow}==> Updating block-device's size: ${dev_name} ${endColor}" &&
         echo "echo 1 > /sys/block/${dev_name}/device/rescan" &&
         echo 1 > /sys/block/${dev_name}/device/rescan
      fi
      echo -e "${yellow}==> Updating physical volume: ${dev_name} ${endColor}" &&
      echo -e "pvresize /dev/$dev_name" &&
      pvresize /dev/$dev_name &&
      pvs /dev/$dev_name
   else
      echo -e "${red}Error: device name not found ${endColor}"
      exit 1
   fi
done

echo -e "${yellow} \n===> Running lvextend Again: ${endColor}"
echo -e "lvextend ${lvextend_options} -r ${lvolume}" 
echo -e "${red}"
lvextend ${lvextend_options} -r ${lvolume} &&
echo -e "${green}Info: Logical volume ${lvolume} has been successfully resized! ${endColor}"
echo -e "${endColor}"
exit 0

